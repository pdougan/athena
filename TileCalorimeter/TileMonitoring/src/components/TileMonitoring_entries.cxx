/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "../TilePaterMonTool.h"
#include "../TileFatherMonTool.h"
#include "../TileDigitsMonTool.h"
#include "../TileRawChannelMonTool.h"
#include "../TileCellNoiseMonTool.h"
#include "../TileTBCellMonTool.h"
#include "../TileJetMonitorAlgorithm.h"
#include "../TileDigitsFlxMonitorAlgorithm.h"
#include "../TileDQFragMonitorAlgorithm.h"
#include "../TileMBTSMonitorAlgorithm.h"
#include "../TileRawChannelFlxMonitorAlgorithm.h"
#include "../TileCellMonitorAlgorithm.h"
#include "../TileTowerMonitorAlgorithm.h"
#include "../TileClusterMonitorAlgorithm.h"
#include "../TileMuIdMonitorAlgorithm.h"
#include "../TileDigiNoiseMonitorAlgorithm.h"
#include "../TileRawChannelTimeMonitorAlgorithm.h"
#include "../TileRawChannelNoiseMonitorAlgorithm.h"
#include "../TileMuonFitMonitorAlgorithm.h"
#include "../TileRODMonitorAlgorithm.h"
#include "../TileTMDBDigitsMonitorAlgorithm.h"
#include "../TileTMDBMonitorAlgorithm.h"
#include "../TileTMDBRawChannelMonitorAlgorithm.h"
#include "../TileDigitsMonitorAlgorithm.h"
#include "../TileRawChannelMonitorAlgorithm.h"
#include "../TileTBPulseMonitorAlgorithm.h"
#include "../TileTBMonitorAlgorithm.h"
#include "../TileTBBeamMonitorAlgorithm.h"

DECLARE_COMPONENT( TileFatherMonTool )
DECLARE_COMPONENT( TilePaterMonTool )
DECLARE_COMPONENT( TileDigitsMonTool )
DECLARE_COMPONENT( TileRawChannelMonTool )
DECLARE_COMPONENT( TileCellNoiseMonTool )
DECLARE_COMPONENT( TileTBCellMonTool )
DECLARE_COMPONENT( TileJetMonitorAlgorithm )
DECLARE_COMPONENT( TileDigitsFlxMonitorAlgorithm )
DECLARE_COMPONENT( TileDQFragMonitorAlgorithm )
DECLARE_COMPONENT( TileMBTSMonitorAlgorithm )
DECLARE_COMPONENT( TileRawChannelFlxMonitorAlgorithm )
DECLARE_COMPONENT( TileCellMonitorAlgorithm )
DECLARE_COMPONENT( TileTowerMonitorAlgorithm )
DECLARE_COMPONENT( TileClusterMonitorAlgorithm )
DECLARE_COMPONENT( TileMuIdMonitorAlgorithm )
DECLARE_COMPONENT( TileDigiNoiseMonitorAlgorithm )
DECLARE_COMPONENT( TileRawChannelTimeMonitorAlgorithm )
DECLARE_COMPONENT( TileRawChannelNoiseMonitorAlgorithm )
DECLARE_COMPONENT( TileMuonFitMonitorAlgorithm )
DECLARE_COMPONENT( TileRODMonitorAlgorithm )
DECLARE_COMPONENT( TileTMDBDigitsMonitorAlgorithm )
DECLARE_COMPONENT( TileTMDBMonitorAlgorithm )
DECLARE_COMPONENT( TileTMDBRawChannelMonitorAlgorithm )
DECLARE_COMPONENT( TileDigitsMonitorAlgorithm )
DECLARE_COMPONENT( TileRawChannelMonitorAlgorithm )
DECLARE_COMPONENT( TileTBPulseMonitorAlgorithm )
DECLARE_COMPONENT( TileTBMonitorAlgorithm )
DECLARE_COMPONENT( TileTBBeamMonitorAlgorithm )
